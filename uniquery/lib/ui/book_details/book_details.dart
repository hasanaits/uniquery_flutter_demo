import 'dart:ui';
import 'package:flutter/material.dart';

class BookDetails extends StatefulWidget {
  @override
  _BookDetails createState() => new _BookDetails();
}

class _BookDetails extends State<BookDetails>{

  void _incrementCounter() {
    setState(() {
      // This call to setState tells the Flutter framework that something has
      // changed in this State, which causes it to rerun the build method below
      // so that the display can reflect the updated values. If we changed
      // _counter without calling setState(), then the build method would not be
      // called again, and so nothing would appear to happen.

    });
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(

      body: new ListView.builder(
          itemCount: 4,
          itemBuilder: (BuildContext context,int index){
            if (index == 0){
              return new Stack(
//                color: Colors.blueGrey,
//                height: 500.0,
//                child: topItem(),
                  children: <Widget>[
                    getBelowContainerOfTop(),
                    getOverlayContainerOfTop()
                  ],
              );
            }else if(index == 1){
              return new Card(
                child: secondItem(),
              );
            }else if (index == 2){
              return new Card(
                child: thirdItem(),
              );
            }else{
              return new Card(
                child: fourthItem(),
              );
            }
          }
      ),
    );
  }



  Widget getBelowContainerOfTop(){
    return new Container(
      child: new Column(

        children: <Widget>[
          new Container(
            height:250.0,
            decoration: new BoxDecoration(
              image: new DecorationImage(
                image: new ExactAssetImage('assets/images/book_betails.png'),
                fit: BoxFit.cover,
              ),
            ),
            child: new BackdropFilter(
              filter: new ImageFilter.blur(sigmaX: 10.0, sigmaY: 10.0),
              child: new Container(
                decoration: new BoxDecoration(color: Colors.white.withOpacity(0.0)),
              ),
            ),
          ),
          new Container(
            color: Colors.white,
            height: 250.0,
          )
        ],
      ),
    );
  }

  Widget getOverlayContainerOfTop(){
    return new Container(
      height: 500.0,
      color: Colors.transparent,
      child: new Column(
        children: <Widget>[
          getAppBar(),
          getAppbarBelowView()

        ],
      ),
    );
  }

  Widget getAppbarBelowView(){
    return new Stack(
      children: <Widget>[
        new Container(
          margin: new EdgeInsets.fromLTRB(30.0, 80.0, 30.0, 0.0),
          height: 344.0,
          child:new Container(
            margin: new EdgeInsets.fromLTRB(6.0, 0.0, 6.0, 0.0),
            color: Colors.transparent,
            child: new BackdropFilter(
              filter: new ImageFilter.blur(sigmaX: 10.0, sigmaY: 10.0),
              child: new Container(
                decoration: new BoxDecoration(
                    color: Colors.white.withOpacity(.8),
                    shape: BoxShape.rectangle,
                    borderRadius: new BorderRadius.circular(8.0),
                    boxShadow: <BoxShadow>[
                      new BoxShadow(
                          color: Colors.black12,
                          //blurRadius: 0.3,
                          offset: new Offset(0.0, 2.0)
                      )
                    ]

                ),
                child: new Center(

                ),
              ),
            ),
          )
        ),
        new Container(
          margin: new EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 0.0),
          child: new Center(
            child: new Padding(
              padding: const EdgeInsets.all(8.0),
              child: new Center(
                child: new Container(
                  height: 160.0,
                  width: 130.0,
                  decoration: new BoxDecoration(
                    image: new DecorationImage(
                      image: new ExactAssetImage('assets/images/book_betails.png'),
                      fit: BoxFit.cover,
                    ),
                    border: new Border.all(
                      color: Colors.red,
                      width: 3.0
                    )
                  ),
                ),
              ),
            ),
          ),
        )
      ],
    );
  }



  Widget getAppBar(){
    return new AppBar(
      backgroundColor: Colors.transparent,
      elevation: 0.0,
      leading: new IconButton(
          icon: new Icon(Icons.arrow_back,
              size:30.0,
              color: Colors.white
          ),
          onPressed: null),
      actions: <Widget>[
        new IconButton(
            icon: new Icon(Icons.search,
              size:30.0,
              color: Colors.white,
            ),

            onPressed: null
        ),
        new IconButton(
            icon: new Icon(Icons.bookmark,
              size:30.0,
              color: Colors.white,),
            onPressed: null
        )
      ],
    );
  }

  Widget secondItem(){
    return new Container(
      padding: const EdgeInsets.all(30.0),
      height: 200.0,
    );
  }
  Widget thirdItem(){
    return new Container(
      padding: const EdgeInsets.all(30.0),
      height: 120.0,
    );
  }

  Widget fourthItem(){
    return new Container(
      padding: const EdgeInsets.all(30.0),
      height: 60.0,
        child: new ClipRect(
          child: new BackdropFilter(
            filter: new ImageFilter.blur(sigmaX: 10.0, sigmaY: 10.0),
            child: new Container(
              width: 200.0,
              height: 200.0,
              decoration: new BoxDecoration(
                  color: Colors.grey.shade200.withOpacity(0.5)
              ),
              child: new Center(
                child: new Text(
                    'Frosted',
                    style: Theme.of(context).textTheme.display3
                ),
              ),
            ),
          ),
        )
    );
  }

}