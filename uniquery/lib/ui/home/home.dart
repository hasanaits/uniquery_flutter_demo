import 'dart:async';
import 'package:flutter/material.dart';

class Home extends StatefulWidget {
  @override
  _Home createState() => new _Home();
}

class _Home extends State<Home>{

  void _incrementCounter() {
    setState(() {
      // This call to setState tells the Flutter framework that something has
      // changed in this State, which causes it to rerun the build method below
      // so that the display can reflect the updated values. If we changed
      // _counter without calling setState(), then the build method would not be
      // called again, and so nothing would appear to happen.

    });
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(

      appBar: new AppBar(
        backgroundColor: Colors.transparent,
        elevation: 0.0,
        leading: new IconButton(
            icon: new Icon(Icons.backspace),
            onPressed: null),
      ),

      body: new ListView.builder(
          itemCount: 4,
          itemBuilder: (BuildContext context,int index){
            if (index == 0){
              return new Card(
                child: topItem(),
              );
            }else if(index == 1){
              return new Card(
                child: secondItem(),
              );
            }else if (index == 2){
              return new Card(
                child: thirdItem(),
              );
            }else{
              return new Card(
                child: fourthItem(),
              );
            }
          }
      ),
    );
  }

  Widget topItem(){
    return new Container(
      color: Colors.indigo,
      padding: const EdgeInsets.all(30.0),
      height: 300.0,
    );
  }

  Widget secondItem(){
    return new Container(
      padding: const EdgeInsets.all(30.0),
      height: 200.0,
    );
  }
  Widget thirdItem(){
    return new Container(
      padding: const EdgeInsets.all(30.0),
      height: 120.0,
    );
  }

  Widget fourthItem(){
    return new Container(
      padding: const EdgeInsets.all(30.0),
      height: 60.0,
    );
  }

}