import 'package:flutter/material.dart';
import 'package:uniquery/ui/book_details//book_details.dart';
import 'package:uniquery/ui/home/home.dart';
import 'package:uniquery/uniquery_color.dart';

void main() {
  runApp(
      new MaterialApp(
        title: "",
        theme: new ThemeData(
          primarySwatch:Colors.indigo,
        ),
        onGenerateRoute: (RouteSettings settings) {
          switch (settings.name) {
            case '/BookDetails':
              return new CustomRoute(
                builder: (_) => new BookDetails(),
                settings: settings,
              );
            case '/Home':
              return new CustomRoute(
                builder: (_) => new Home(),
                settings: settings,
              );

          }
          assert(false);
        },
        home: new BookDetails(),
      )
  );
}

class CustomRoute<T> extends MaterialPageRoute<T> {
  CustomRoute({ WidgetBuilder builder, RouteSettings settings })
      : super(builder: builder, settings: settings);

  @override
  Widget buildTransitions(BuildContext context,
      Animation<double> animation,
      Animation<double> secondaryAnimation,
      Widget child) {
    if (settings.isInitialRoute)
      return child;
    return new FadeTransition(opacity: animation, child: child);
  }
}


